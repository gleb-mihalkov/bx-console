!(function($) {

  if ($ == null) {
    console.error('jQuery is required');
    return;
  }

  /**
   * Обрабатывает событие поступления значений на отображение.
   * @param  {Event} e Событие.
   * @return {void}
   */
  function onConsole(e) {
    var dump = e.value;

    for (var i = 0; i < dump.length; i++) {
      var frame = dump[i];
      console.group(frame.label);
      console.log(frame.text);
      console.groupEnd();
    }
  }

  // Прикрепляем обработчики.
  $(window).on('consoleDump.phpTransport', onConsole);

})(window.jQuery);
