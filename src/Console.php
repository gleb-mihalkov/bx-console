<?php
namespace Bx\Console;
use Bx\JsTransport\JsTransport;
use Bx\Service\Service;
use Bitrix\Main\Page\Asset;
use stdClass;

/**
 * Выводит сообщения в консоль веб-инспектора браузера.
 */
class Console extends Service
{
    /**
     * Уровень трассировки стека вызовов, на котором должен находиться вызов метода
     * отобржения дампа.
     * @var integer
     */
    const DUMP_BACKTRACE_LEVEL = 4;



    /**
     * Выполняется при инициализации сервиса.
     * @return void
     */
    protected function boot()
    {
        JsTransport::init();

        $isIncludeHandler = JsTransport::isRequest()
            && JsTransport::isHtmlRequest()
            && !JsTransport::isAjaxRequest();

        if (!$isIncludeHandler)
        {
            return;
        }

        $this->bind('OnEpilog', 'onFinish');
    }

    /**
     * Обрабатывает конец загрузки страницы.
     * @return void
     */
    protected function onFinish()
    {
        $this->addHandlerJavascript();
    }



    /**
     * Возвращает JS-код обработчика консоли.
     * @return string JS-код обработчика.
     */
    private function getHandlerJavascript() : string
    {
        $fileRaw = __DIR__.'/ConsoleHandler.js';
        $fileMin = __DIR__.'/ConsoleHandler.min.js';

        return file_exists($fileMin)
            ? file_get_contents($fileMin)
            : file_get_contents($fileRaw);
    }

    /**
     * Добавляет JS-код обработчика консоли в тег HEAD страницы.
     */
    private function addHandlerJavascript()
    {
        $content = $this->getHandlerJavascript();
        $script = '<script type="text/javascript">'.$content.'</script>';

        $asset = Asset::getInstance();
        $asset->addString($script);
    }



    /**
     * Возвращает дамп значения.
     * @param  mixed  $value Значение.
     * @return string        Дамп значения.
     */
    private function getDumpText($value) : string
    {
        ob_start();
        var_dump($value);
        $dump = ob_get_contents();
        ob_end_clean();

        return $dump;
    }

    /**
     * Получает номер версии PHP, установленной на сервере.
     * @return string Номер версии.
     */
    private function getPhpVersion() : string
    {
        $version = phpversion();

        $match = [];
        preg_match('/[0-9]+\.[0-9]+(\.[0-9])?/', $version, $match);

        $version = $match[0];
        return $version;
    }

    /**
     * Возвращает текстовую запись текущей метки времени.
     * @return string Метка времени.
     */
    private function getCurrentTime() : string
    {
        $timeFull = microtime(true);
        $timeInt = floor($timeFull);
        $timeFloat = round(fmod($timeFull, 1), 3) * 1000;

        $date = date('Y/m/d H:i:s', $timeInt);
        $time = $date.'.'.$timeFloat;

        return $time;
    }

    /**
     * Получает заголовок для дампа.
     * @param  int    $level Задает уровень трассировки стека вызовов, с которого
     *                       следует получать имя файла и номер строки.
     * @return string        Заголовок для дампа.
     */
    private function getDumpLabel(int $level) : string
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $level);
        $frame = array_pop($trace);

        $version = $this->getPhpVersion();
        $timestamp = $this->getCurrentTime();

        $label = sprintf('PHP %s [%s]: %s:%d', $version, $timestamp, $frame['file'], $frame['line']);
        return $label;
    }

    /**
     * Выводит в консоль браузера дамп указанного значения.
     * @param  mixed $value Значение.
     * @param  int   $level Уровень трассировки стека, с которого следует брать
     *                      имя файла и номер строки вызова этого метода.
     * @return void
     */
    protected function dump($value, ?int $level = null)
    {
        $dump = JsTransport::get('consoleDump') ?? [];

        $level = $level ?? self::DUMP_BACKTRACE_LEVEL;

        $label = $this->getDumpLabel($level);
        $text = $this->getDumpText($value);

        $value = new stdClass();
        $value->label = $label;
        $value->text = $text;

        $dump[] = $value;
        JsTransport::set('consoleDump', $dump);
    }




    /**
     * Возвращает текст с трассировкой стека вызовов.
     * @param  int    $level Уровень трассировки, на котором произошел вызов метода trace().
     * @return string        Текст с трассировкой стека вызовов.
     */
    private function getTraceText(int $level) : string
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $trace = array_slice($trace, $level - 1);
        $lines = [];

        foreach ($trace as $frame)
        {
            $file = $frame['file'] ?? null;
            $line = $frame['line'] ?? null;
            $class = $frame['class'] ?? null;
            $func = $frame['function'] ?? null;
            $callType = $frame['type'] ?? null;

            $call = ($class ? $class.$callType.$func : $func).'()';
            $file = $file.':'.$line;
            $line = $file.PHP_EOL.' → '.$call;

            $lines[] = $line;
        }

        $trace = $lines
            ? implode(PHP_EOL.PHP_EOL, $lines)
            : 'Backtrace is empty';

        return $trace;
    }

    /**
     * Выводит трассировку стека вызовов в консоль браузера.
     * @param  int  $level Уровень трассировки стека, с которого произошел вызов данного
     *                     метода.
     * @return void
     */
    protected function trace(?int $level = null)
    {
        $dump = JsTransport::get('consoleDump') ?? [];

        $level = $level ?? self::DUMP_BACKTRACE_LEVEL;

        $label = $this->getDumpLabel($level);
        $text = $this->getTraceText($level);

        $value = new stdClass();
        $value->label = $label;
        $value->text = $text;

        $dump[] = $value;
        JsTransport::set('consoleDump', $dump);
    }
}
