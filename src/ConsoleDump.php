<?php
use Bx\Console\Console;

/**
 * Отображает дамп значения в консоли отладчика браузера.
 * @param  mixed $value Значение.
 * @return void
 */
function console_dump($value)
{
    $traceLevel = Console::DUMP_BACKTRACE_LEVEL;
    Console::dump($value, $traceLevel + 1);
}
